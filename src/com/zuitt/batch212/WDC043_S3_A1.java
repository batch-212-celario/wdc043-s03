package com.zuitt.batch212;

import java.util.Scanner;

public class WDC043_S3_A1 {
    public static void main(String[] args) {
        Scanner inputNumber = new Scanner(System.in);

        System.out.println("Input an integer whose factorial will be computed");
        //inputNumber.nextInt();
         int num = 0;
         num = inputNumber.nextInt();
        long factorial = 1;
        for(int i = 1;i <= num; ++i ){
            factorial *= i;
        }
        System.out.println("Factorial of " + num + " " + "is " + factorial);
    }

}
